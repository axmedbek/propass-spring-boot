package az.pronet.propass.controller.auth;

import az.pronet.propass.exception.InvalidCredentialsException;
import az.pronet.propass.model.jwt.JwtRequest;
import az.pronet.propass.model.jwt.JwtResponse;
import az.pronet.propass.service.jwt.JwtUserDetailsService;
import az.pronet.propass.util.JwtTokenUtil;
import az.pronet.propass.util.SuccessResponse;
import az.pronet.propass.util.ValidationResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/auth")
public class JwtAuthenticationController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final JwtUserDetailsService userDetailsService;

    public JwtAuthenticationController(
            AuthenticationManager authenticationManager,
            JwtTokenUtil jwtTokenUtil,
            JwtUserDetailsService userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(
            @Valid @RequestBody(required = false) JwtRequest authenticationRequest,
            BindingResult bindingResult) throws Exception {

        if (authenticationRequest == null){
            return ResponseEntity.ok(new ValidationResponse(bindingResult));
        }
        else{
            if (bindingResult.hasErrors()) {
                return ResponseEntity.ok(new ValidationResponse(bindingResult));
            } else {
                authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
                final UserDetails userDetails = userDetailsService
                        .loadUserByUsername(authenticationRequest.getUsername());
                final String token = jwtTokenUtil.generateToken(userDetails);
                return ResponseEntity.ok(new JwtResponse(token));
            }
        }
//        try{
//            if (bindingResult.hasErrors()) {
//                return ResponseEntity.ok(new ValidationResponse(bindingResult));
//            } else {
//                authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
//                final UserDetails userDetails = userDetailsService
//                        .loadUserByUsername(authenticationRequest.getUsername());
//                final String token = jwtTokenUtil.generateToken(userDetails);
//                return ResponseEntity.ok(new JwtResponse(token));
//            }
//        }
//        catch (Exception e){
//            return ResponseEntity.ok(e.getMessage());
//        }
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        }
    }
}
