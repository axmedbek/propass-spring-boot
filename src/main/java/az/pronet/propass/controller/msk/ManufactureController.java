package az.pronet.propass.controller.msk;

import az.pronet.propass.dto.msk.ManufactureDTO;
import az.pronet.propass.exception.RecordNotFoundException;
import az.pronet.propass.service.msk.ManufactureService;
import az.pronet.propass.util.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(ApiPaths.API_PATH + "/msk/manufacture")
@CrossOrigin
public class ManufactureController {

    private final ManufactureService manufactureService;

    public ManufactureController(ManufactureService manufactureService) {
        this.manufactureService = manufactureService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<ManufactureDTO>> getAllManufactures() {
        List<ManufactureDTO> manufactures = manufactureService.getAllManufactures();
        return ResponseEntity.ok(manufactures);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<ManufactureDTO> getManufactureById(@PathVariable("id") Long id) throws RecordNotFoundException {
        ManufactureDTO manufacture = manufactureService.getManufactureById(id);
        return ResponseEntity.ok(manufacture);
    }

    @PostMapping("/save")
    @ResponseBody
    public Object saveManufacture(@Valid ManufactureDTO manufacture,
                                  BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            this.manufactureService.saveManufacture(manufacture);
            return new SuccessResponse(true, "Successfully added");
        } else {
            return new ValidationResponse(bindingResult);
        }
    }

    //    edit manufacture
    @PutMapping("/edit/{id}")
    @ResponseBody
    public Object editManufacture(@PathVariable("id") Long id, @Valid ManufactureDTO manufacture,
                                  BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            return new SuccessResponse(true, "Deleted successfully");
        } else {
            return new ValidationResponse(bindingResult);
        }
    }

    //    delete manufacture
    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public Object deleteManufacture(@PathVariable("id") Long id) {
        Long deleted_id = this.manufactureService.deleteManufactureById(id);
        System.out.println(deleted_id);
        return new SuccessResponse(true, "Deleted successfully");
    }
}
