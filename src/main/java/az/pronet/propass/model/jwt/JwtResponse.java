package az.pronet.propass.model.jwt;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;

    private final String jwt_token;

    public JwtResponse(String jwt_token) {
        this.jwt_token = jwt_token;
    }
    public String getToken() {
        return this.jwt_token;
    }
}
