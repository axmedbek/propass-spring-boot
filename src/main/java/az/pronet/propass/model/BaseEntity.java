package az.pronet.propass.model;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@MappedSuperclass
public abstract class BaseEntity {
    @CreatedDate
    private Timestamp created_at;
    @CreatedBy
    private Long created_by;
    @LastModifiedDate
    private Timestamp updated_at;
    @LastModifiedBy
    private Long updated_by;
    private Timestamp deleted_at;
    private Long deleted_by;
}
