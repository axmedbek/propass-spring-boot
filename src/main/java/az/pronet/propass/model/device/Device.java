package az.pronet.propass.model.device;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "devices")
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private Long manufacture_id;
    private Long model_id;
    private Long destination_id;
    private String ip_address;
    private Long serial_number;
    private String google_map;
    private String note;
    private Boolean used;
    private Long location_id;
    private Long fp_id;
}
