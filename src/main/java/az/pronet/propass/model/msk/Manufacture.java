package az.pronet.propass.model.msk;

import az.pronet.propass.model.Auditable;

import lombok.*;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "manufactures")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Where(clause = "deleted_date IS NULL")
//@SQLDelete(sql = "UPDATE manufactures SET deleted_date = current_timestamp WHERE id = ?")
public class Manufacture extends Auditable<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull(message = "Name is required")
    private String name;

    public Manufacture(String name){
        this.name = name;
    }
}
