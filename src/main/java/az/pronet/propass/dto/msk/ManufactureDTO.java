package az.pronet.propass.dto.msk;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManufactureDTO {
    private Long id;
    private String name;
    private Date created_date;
}
