package az.pronet.propass.exception;

import az.pronet.propass.util.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice(annotations = {RestController.class})
public class UncaughtExceptionsControllerAdvice {
    @ExceptionHandler({MethodArgumentNotValidException.class, HttpMessageNotReadableException.class})
    public ResponseEntity handleBindingErrors(Exception ex) {
        ErrorResponse exceptionResponse = new ErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                "Validation Error",
                new Timestamp(System.currentTimeMillis()),
                "Oops...Bad Request."
        );
        return ResponseEntity.badRequest().body(exceptionResponse);
    }
}
