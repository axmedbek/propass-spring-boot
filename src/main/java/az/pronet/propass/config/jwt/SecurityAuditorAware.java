package az.pronet.propass.config.jwt;
import az.pronet.propass.model.jwt.PPassUserDetail;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@Configuration
@EnableJpaAuditing
public class SecurityAuditorAware implements AuditorAware<Long> {
    @Override
    public Optional<Long> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.empty();
        }
        PPassUserDetail passUserDetail = (PPassUserDetail)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Optional.of(passUserDetail.getUserId());
    }
}
