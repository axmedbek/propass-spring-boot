package az.pronet.propass.service.user.impl;

import az.pronet.propass.model.user.User;
import az.pronet.propass.respository.user.UserRepository;
import az.pronet.propass.service.user.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component("userServiceImpl")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void saveRootUser() {
        User admin = new User();
        admin.setUsername("propass");
        admin.setPassword(passwordEncoder.encode("123456"));
        this.userRepository.save(admin);
    }

    @Override
    public User findByUsername(String username) {
        return this.userRepository.findByUsername(username);
    }
}
