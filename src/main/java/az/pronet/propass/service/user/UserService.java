package az.pronet.propass.service.user;

import az.pronet.propass.model.user.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    void saveRootUser();
    User findByUsername(String username);
}
