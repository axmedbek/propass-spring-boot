package az.pronet.propass.service.jwt;

import az.pronet.propass.model.jwt.PPassUserDetail;
import az.pronet.propass.model.user.User;
import az.pronet.propass.respository.user.UserRepository;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public JwtUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public PPassUserDetail loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findByUsername(username);
        if (user != null) {
            return new PPassUserDetail(
                    user.getUsername(),
                    user.getPassword(),
                    null,
                    true,
                    true,
                    true,
                    true,
                    user.getId()
            );
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }
}
