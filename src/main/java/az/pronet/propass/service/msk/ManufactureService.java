package az.pronet.propass.service.msk;

import az.pronet.propass.dto.msk.ManufactureDTO;
import az.pronet.propass.exception.RecordNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ManufactureService {

    public List<ManufactureDTO> getAllManufactures();
    public ManufactureDTO getManufactureById(Long id) throws RecordNotFoundException;
    public void saveManufacture(ManufactureDTO manufactureDTO);
    public Long deleteManufactureById(Long id);
}
