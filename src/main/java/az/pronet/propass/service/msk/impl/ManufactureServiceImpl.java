package az.pronet.propass.service.msk.impl;

import az.pronet.propass.dto.msk.ManufactureDTO;
import az.pronet.propass.exception.RecordNotFoundException;
import az.pronet.propass.model.msk.Manufacture;
import az.pronet.propass.respository.msk.ManufactureRepository;
import az.pronet.propass.service.msk.ManufactureService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component("manufactureServiceImpl")
public class ManufactureServiceImpl implements ManufactureService {

    private final ManufactureRepository manufactureRepository;
    private final ModelMapper modelMapper;

    public ManufactureServiceImpl(ManufactureRepository manufactureRepository,ModelMapper modelMapper) {
        this.manufactureRepository = manufactureRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<ManufactureDTO> getAllManufactures() {
        List<Manufacture> manufactureList = manufactureRepository.findAll();
        return manufactureList.stream()
                .map(entity -> modelMapper.map(entity, ManufactureDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public ManufactureDTO getManufactureById(Long id) throws RecordNotFoundException {
        Optional<Manufacture> employee = manufactureRepository.findById(id);
        if (employee.isPresent()) {
            return modelMapper.map(employee.get(),ManufactureDTO.class);
        } else {
            throw new RecordNotFoundException("Manufacture not found with given id : " + id);
        }
    }

    @Override
    public void saveManufacture(ManufactureDTO manufactureDTO) {
        Manufacture manufacture = new Manufacture(manufactureDTO.getName());
        manufactureRepository.save(manufacture);
    }

    @Override
    @Transactional
    public Long deleteManufactureById(Long id) {
        return manufactureRepository.softDelete(id);
    }
}
