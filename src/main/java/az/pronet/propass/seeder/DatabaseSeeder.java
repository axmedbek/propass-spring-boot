package az.pronet.propass.seeder;

import org.springframework.stereotype.Component;

@Component
public class DatabaseSeeder {

    private final ManufactureSeeder manufactureSeeder;

    public DatabaseSeeder(ManufactureSeeder manufactureSeeder) {
        this.manufactureSeeder = manufactureSeeder;
    }

    public void run(){
        manufactureSeeder.seed();
    }

}
