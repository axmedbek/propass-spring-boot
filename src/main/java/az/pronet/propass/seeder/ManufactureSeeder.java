package az.pronet.propass.seeder;

import az.pronet.propass.model.msk.Manufacture;
import az.pronet.propass.respository.msk.ManufactureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ManufactureSeeder {
    private final ManufactureRepository manufactureRepository;

    public ManufactureSeeder(ManufactureRepository manufactureRepository) {
        this.manufactureRepository = manufactureRepository;
    }

    void seed() {
        Manufacture manufacture = new Manufacture();
        manufacture.setName("Manufacture1");
        manufacture.setCreatedBy(1L);
        manufacture.setLastModifiedBy(1L);
        manufactureRepository.save(manufacture);
    }
}
