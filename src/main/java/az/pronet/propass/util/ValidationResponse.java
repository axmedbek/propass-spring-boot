package az.pronet.propass.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ValidationResponse {
    private int status = HttpStatus.BAD_REQUEST.value();
    private Map<String,String> errors;

    public ValidationResponse(BindingResult bindingResult){
        List<FieldError> errors = bindingResult.getFieldErrors();
        Map<String, String> message = new HashMap<>();
        for (FieldError e : errors) {
            message.put(e.getField(), e.getDefaultMessage());
        }
        this.errors = message;
    }
}
