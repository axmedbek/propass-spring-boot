package az.pronet.propass.util;

import az.pronet.propass.model.jwt.PPassUserDetail;
import org.springframework.security.core.context.SecurityContextHolder;

import java.sql.Date;
import java.sql.Timestamp;

public class StaticValues {
    public static final Timestamp CURRENTTIME = new Timestamp(System.currentTimeMillis());

    public static Long getCurrentUserId(){
        Long user_id = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof PPassUserDetail) {
            user_id = ((PPassUserDetail)principal).getUserId();
        }
        return user_id;
    }
}

