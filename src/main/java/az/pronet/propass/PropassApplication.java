package az.pronet.propass;

import az.pronet.propass.seeder.DatabaseSeeder;
import az.pronet.propass.service.user.UserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
public class PropassApplication {

	private final UserService userService;
	private final DatabaseSeeder databaseSeeder;

	public PropassApplication(UserService userService, DatabaseSeeder databaseSeeder) {
		this.userService = userService;
		this.databaseSeeder = databaseSeeder;
	}

	public static void main(String[] args) {
		SpringApplication.run(PropassApplication.class, args);
	}

	@EventListener
	public void seed(ContextRefreshedEvent event) {
		this.userService.saveRootUser();
		this.databaseSeeder.run();
	}

	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

}
