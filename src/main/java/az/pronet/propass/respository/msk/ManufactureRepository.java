package az.pronet.propass.respository.msk;

import az.pronet.propass.model.msk.Manufacture;
import az.pronet.propass.util.StaticValues;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public interface ManufactureRepository extends JpaRepository<Manufacture, Long>,ManufactureExtended {

//    @Modifying
//    @Query("update Manufacture m set m.deletedBy = :deleted_by where m.id = :id")
//    void softDelete(@Param("id") Long id,@Param("deleted_by") Long deleted_by);

}


interface  ManufactureExtended {
    Long softDelete(Long id);
}

class ManufactureExtendedImpl implements ManufactureExtended {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Long softDelete(Long id){
        String queryString = "UPDATE manufactures SET deleted_date = ?2 , deleted_by = ?3 WHERE id = ?1";
        Query query = entityManager.createNativeQuery(queryString);
        query.setParameter(1,id);
        query.setParameter(2, StaticValues.CURRENTTIME);
        query.setParameter(3, StaticValues.getCurrentUserId());
        return (long) query.executeUpdate();
    }
}
