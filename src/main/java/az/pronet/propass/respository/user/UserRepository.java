package az.pronet.propass.respository.user;

import az.pronet.propass.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

// main repo
@Repository
public interface UserRepository extends JpaRepository<User,Long>, userRepositoryExtended {
    User findByUsername(String username);
}


// implementations for custom repo
@Repository
@Transactional(readOnly = true)
class UserRepositoryExtendedImpl implements userRepositoryExtended {

}


// interfaces for custom methods
interface userRepositoryExtended {

}
